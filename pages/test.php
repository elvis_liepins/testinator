<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    include('includes/head.html');
    ?>
    <script src="resources/test.js"></script> 
</head>
<body>
    <div class="container">
        <form id="testSubmit" action="result.php" method="post">
            <?php
            //creates div for each question, only first visible
            foreach ($questions as $index => $question) {
                $class = ($index == 0) ? 'question-block-active' : 'question-block';
                echo "<div class=\"$class\">";
                echo "<h2>$question->text</h2>";
                
                echo '<div class="grid">';
                foreach ($question->answers as $answer) {
                    echo '<div class="answer-block">';
                    echo "<input type=\"radio\" name=\"$question->id\" id=\"answer$answer->id\" value=\"$answer->id\">";
        
                    echo "<label for=\"answer$answer->id\">$answer->text</label>";
                    echo '</div>';
                }
                echo '</div>';
                echo '</div>';
            }
            ?>
            <div class="center">
                <progress id="testProgress" value="1" max="<?php echo count($questions) ?>"></progress>
            </div>
            <div class="center">
                <button id="nextBtn" type="button" onclick="submitAnswer()">
                    <?php echo count($questions) == 1 ? 'Iesniegt' : 'Nākamais' ?>
                </button>
            </div>
        </form>
    </div>
</body>
</html>