<?php
require('../config.php');
require_once('../classes/Result.php');
require_once('../helpers.php');

session_start();
dieIfEmpty($_SESSION['name']);
dieIfEmpty($_SESSION['testId']);
$name = $_SESSION['name'];
$test_id = intval($_SESSION['testId']);

$db = new Result($conf->db);
$db->dieIfConnectError();
$grade = $db->getGrade($test_id, $_POST);
$db->saveResults($name);

session_unset();
session_destroy();

require_once('../pages/result.php');
