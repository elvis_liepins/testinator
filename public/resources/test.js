function submitAnswer() {
    let div = document.getElementsByClassName('question-block-active')[0]
    if (div.querySelector('input:checked') === null) return

    let nextDivs = document.getElementsByClassName('question-block')

    if (nextDivs.length === 1) {
        document.getElementById('nextBtn').innerText = 'Iesniegt'
    } else if (nextDivs.length === 0) {
        document.getElementById('testSubmit').submit()
    }

    div.className = 'question-block-answered'
    nextDivs[0].className = 'question-block-active'

    document.getElementById('testProgress').value++
}