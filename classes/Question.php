<?php
require_once('Database.php');

class Question extends DataBase
{
    public function getQuestionsOrDie($id)
    {
        if ($id == 0) {
            $this->die('Tests nav pieejams');
        }
        $query = 'SELECT id, text FROM questions  
                  WHERE questions.test_id ="' . $id . '"';
        $questionsResult = $this->connection->query($query);
        if ($questionsResult->num_rows == 0) {
            $this->die('Tests nav pieejams');
        }

        //gets questions and answers as objects in array
        $questions = array();
        while ($question = $questionsResult->fetch_object()) {
            $answerQuery = 'SELECT id, text FROM answers WHERE question_id = ' . $question->id;
            $answersResult = $this->connection->query($answerQuery);
            $answers = array();
            while ($answer = $answersResult->fetch_object()) {
                $answers[] = $answer;
            }
            $question->answers = $answers;
            $questions[] = $question;
        }
        return $questions;
    }
}
