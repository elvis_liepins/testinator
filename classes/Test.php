<?php
require_once('Database.php');

class Test extends DataBase
{
    public function getTestsOrDie()
    {
        $query = "SELECT * FROM tests";
        $tests = $this->connection->query($query);
        if ($tests->num_rows === 0) {
            $this->die();
        }
        return $tests;
    }
}
