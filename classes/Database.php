<?php
class DataBase
{
    protected $connection = null;
    protected $error_message = 'Testi pašlaik nav pieejami';

    public function __construct($db)
    {
        $this->connection = new mysqli($db->host, $db->username, $db->password, $db->database);
        $this->connection->set_charset('utf8');
    }

    protected function die($msg = null)
    {
        $error_message = empty($msg) ? $this->error_message : $msg;
        require_once('../pages/error.php');
        die;
    }

    public function dieIfConnectError()
    {
        if ($this->connection->connect_errno) {
            $this->die();
        }
    }
}
