<!DOCTYPE html>
<html lang="en">
<head>
<?php
    include('includes/head.html');
?>
</head>
<body>
    <div class="container">
        <form action="test.php" method="post">
            <h1>Testinator</h1>
            <?php
            if (!empty($error)) {
                echo "<p class=\"error\">$error</p>";
            }
            ?>
            <input type="text" name="name" id="name" placeholder="Ievadi savu vārdu" required>
            <label for="testId">Izvēlies testu</label>
            <select name="testId" id="testId">
                <?php
                while ($test = $tests->fetch_object()) {
                    echo "<option value=\"$test->id\">$test->name</option>";
                }
                ?>
            </select>
            <input type="submit" value="Sākt">     
        </form>
    </div>
</body>
</html>