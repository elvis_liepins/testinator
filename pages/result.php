<!DOCTYPE html>
<html lang="en">
<head>
<?php
    include('includes/head.html');
?>
</head>
<body>
    <div class="container center">
        <h1><?php echo $name ?></h1>
        <p>
            Jūs pareizi atbildējāt uz 
            <?php echo $grade->points . ' no ' . $grade->max . ($grade->max == 1 ? ' jautājuma.' : ' jautājumiem.') ?>
        </p>
        <div class="center">
            <a href="index.php">Atgriezties.</a>
        </div>
    </div>
</body>
</html>