<?php
require('../config.php');
require_once('../classes/Question.php');
require_once('../helpers.php');

session_start();
dieIfEmpty($_POST['name'], 'Ievadi vārdu!');
dieIfEmpty($_POST['testId']);

$id = intval($_POST['testId']);

$db = new Question($conf->db);
$db->dieIfConnectError();
$questions = $db->getQuestionsOrDie($id);

$_SESSION['name'] = $_POST['name'];
$_SESSION['testId'] = $id;

require('../pages/test.php');
