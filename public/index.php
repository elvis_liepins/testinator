<?php
require('../config.php');
require_once('../classes/Test.php');

$test_data = new Test($conf->db);
$test_data->dieIfConnectError();
$tests = $test_data->getTestsOrDie();

require_once('../pages/index.php');
