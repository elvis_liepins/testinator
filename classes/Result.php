<?php
require_once('Database.php');

class Result extends DataBase
{
    private $max = 0;
    private $points = 0;
    private $test_id;

    private function getCorrectAnswers($test_id)
    {
        $query = 'SELECT qa.question_id, qa.answer_id FROM tests
                  INNER JOIN questions ON tests.id = questions.test_id
                  INNER JOIN question_answer AS qa ON questions.id = qa.question_id
                  WHERE tests.id =' . $test_id;
        $answers = $this->connection->query($query);
        $this->max = $answers->num_rows;
        return $answers->fetch_all(MYSQLI_ASSOC);
    }

    public function getGrade($test_id, $guesses)
    {
        $answers = $this->getCorrectAnswers($test_id);
        $this->test_id = $test_id;

        foreach ($answers as $answer) {
            $answer = (object) $answer;
            if (isset($guesses[$answer->question_id]) && $answer->answer_id === $guesses[$answer->question_id]) {
                $this->points++;
            }
        }
        $grade = (object) ['points' => $this->points, 'max' => $this->max];
        return $grade;
    }

    public function saveResults($name)
    {
        $name = $this->connection->real_escape_string($name);
        $query = sprintf("INSERT INTO results (name, test_id, points, max_points)
                          VALUES ('%s', %d, %d, %d)", $name, $this->test_id, $this->points, $this->max);
        $this->connection->query($query);
    }
}
